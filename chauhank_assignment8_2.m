%% CREATE DATA FOR SPECTRAL CLUSTERING

load 'BostonListing.mat'

X = horzcat(latitude, longitude);
y = categorical(neighbourhood);

% sigma in similarity metric
sigma = 0.01;

% pairwise squared euclidean distance matrix
sq_euclideans = squareform(pdist(X, 'squaredeuclidean'));

% similarity/weighted adjacency matrix
sim_mat = exp( sq_euclideans/(-2*(sigma^2)) );

% adjacency matrix is ones since fully connected
adj_mat = ones(size(X,1));


%% CALCULATE PURITY METRIC FOR VARIOUS K

% purity metric values for each k
purity = zeros(25,1);

for k = 1:25
    
    % spectral clustering using symmetrically normalized laplacian
    [assignments, ~, ~, ~] = sc3(sim_mat, adj_mat, k);
    
    % calculate ni for each cluster i
    ni = 0;
    for c_num = 1:k
        ni = ni + sum(y(assignments==c_num) == mode( y(assignments==c_num) ));
    end
    
    % update value in purity vector to be sum of ni
    purity(k,1) = ni;
        
end

% divide each sum(ni) by n
purity = purity/size(X,1);


%% PLOT PURITTY VS K

figure;
plot(1:numel(purity), purity, '-o');
ax = gca;
ax.Title.String = 'Clustering Purity Metric for various k';
ax.XLabel.String = 'k';
ax.YLabel.String = 'Purity';


%% CLUSTER WITH K=5 AND PLOT ON GOOGLE MAPS

% k=5 spectral clustering using symmetrically normalized laplacian
[assignments, ~, ~, ~] = sc3(sim_mat, adj_mat, 5);

% plot on google map
for c_num = 1:5
    plot(longitude(assignments==c_num), latitude(assignments==c_num), '.', 'MarkerSize', 10);
    hold on;
end
plot_google_map;
