function [assignments, centroids, eigenvectors, eigenvalues] = sc3(sim_mat, adj_mat, k, seed)
    
    % if seed for rng is not specified, set it to 2
    if ~exist('seed','var')
        seed = 2;
    end

    % calculate weighted adjacency matrix
    w_adj = sim_mat .* adj_mat;
    
    % calculate degree matrix
    D = diag(sum(w_adj));
    
    % graph laplacian
    L = D - w_adj;
    
    % symmetric normalized laplacian
    D_invrt = D^-0.5;
    L_sym = D_invrt * L * D_invrt;
    
    if ~exist('k','var')
        
        assignments = [];
        centroids = [];
        
        % return all the eigenvectors and eigenvalues
        [eigenvectors, eigenvalues] = eig(L_sym);
        
    else
    
        % compute the k smallest eigenvalues and corresponding eignvectors
        [eigenvectors, eigenvalues] = eigs(L_sym, k, 'SM');

        % normalize rows of eigenvectors matrix
        eigenvectors = normr(eigenvectors);

        % for consistency
        rng(seed);

        % run kmeans on the rows of eigenvectors matrix
        [assignments, centroids] = kmeans(eigenvectors, k);
        
    end

end