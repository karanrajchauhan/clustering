%% LOAD DATA

% for reproducability
rng(2);

% generate circle data with 3 clusters, 500 pts per cluster
[D1, D3_labels] = sample_circle(3, [500 500 500]);

% convert to polar coordinates
D3 = zeros(size(D1));
[D3(:,1), D3(:,2)] = cart2pol(D1(:,1), D1(:,2));

% normalize data to be in [0 1]
for col_num = 1:size(D3, 2)
    colmax = max(D3(:,col_num));
    colmin = min(D3(:,col_num));
    D3(:,col_num) = (D3(:,col_num) -  colmin) / (colmax - colmin);
end


%% CLUSTER USING K-MEANS

% k-means with 2 clusters
[k2_assignments, k2_centroids] = kmeans(D3, 2, 'Distance','cityblock', ...
    'Replicates',20);

% k-means with 3 clusters
[k3_assignments, k3_centroids] = kmeans(D3, 3, 'Distance','cityblock', ...
    'Replicates',20);

% k-means with 4 clusters
[k4_assignments, k4_centroids] = kmeans(D3, 4, 'Distance','cityblock', ...
    'Replicates',20);


%% GENERATE PLOTS

% plot k=2 assignments and centroids
figure;
scatter(D3(k2_assignments==1, 1), D3(k2_assignments==1, 2), 'filled', 'r');
hold on;
scatter(D3(k2_assignments==2, 1), D3(k2_assignments==2, 2), 'filled', 'b');
hold on;
scatter(k2_centroids(:,1), k2_centroids(:,2), 50, '*', 'k');

% set plot properties
legend('Cluster 1', 'Cluster 2', 'Cluster Centroids');
ax = gca;
ax.Title.String = 'k-means (k=2) on Transformed Data';
ax.XLabel.String = 'x1';
ax.YLabel.String = 'x2';

% plot k=3 assignments and centroids
figure;
scatter(D3(k3_assignments==1, 1), D3(k3_assignments==1, 2), 'filled', 'r');
hold on;
scatter(D3(k3_assignments==2, 1), D3(k3_assignments==2, 2), 'filled', 'b');
hold on;
scatter(D3(k3_assignments==3, 1), D3(k3_assignments==3, 2), 'filled', 'g');
hold on;
scatter(k3_centroids(:,1), k3_centroids(:,2), 50, '*', 'k');

% set plot properties
legend('Cluster 1', 'Cluster 2', 'Cluster 3', 'Cluster Centroids');
ax = gca;
ax.Title.String = 'k-means (k=3) on Transformed Data';
ax.XLabel.String = 'x1';
ax.YLabel.String = 'x2';

% plot k=4 assignments and centroids
figure;
scatter(D3(k4_assignments==1, 1), D3(k4_assignments==1, 2), 'filled', 'r');
hold on;
scatter(D3(k4_assignments==2, 1), D3(k4_assignments==2, 2), 'filled', 'b');
hold on;
scatter(D3(k4_assignments==3, 1), D3(k4_assignments==3, 2), 'filled', 'g');
hold on;
scatter(D3(k4_assignments==4, 1), D3(k4_assignments==4, 2), 'filled', 'k');
hold on;
scatter(k4_centroids(:,1), k4_centroids(:,2), 50, '*', 'k');

% set plot properties
legend('Cluster 1', 'Cluster 2', 'Cluster 3', 'Cluster 4', 'Cluster Centroids');
ax = gca;
ax.Title.String = 'k-means (k=4) on Transformed Data';
ax.XLabel.String = 'x1';
ax.YLabel.String = 'x2';


%% WITHIN CLUSTER SUM OF DISTANCES

k2_wcsd = zeros(2,1);
for c_num = 1:2
    k2_wcsd(c_num,1) = sum(sum( abs(D3(k2_assignments==c_num, :) - k2_centroids(c_num, :)), 2));
end

k3_wcsd = zeros(3,1);
for c_num = 1:3
    k3_wcsd(c_num,1) = sum(sum( abs(D3(k3_assignments==c_num, :) - k3_centroids(c_num, :)), 2));
end

k4_wcsd = zeros(4,1);
for c_num = 1:4
    k4_wcsd(c_num,1) = sum(sum( abs(D3(k4_assignments==c_num, :) - k4_centroids(c_num, :)), 2));
end
