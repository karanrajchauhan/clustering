%% LOAD DATA

% for consistency in results
rng(2);

% generate circle data with 3 clusters, 500 pts per cluster
[cData, cLabels] = sample_circle(3, [500 500 500]);

% generate spiral data with 3 clusters, 500 pts per cluster
[sData, sLabels] = sample_spiral(3, [500 500 500]);


%% CREATE GRAPH DEFINING MATRICES

% sigma in similarity metric
sigma = 0.2;

% pairwise squared euclidean distance matrices
c_sq_euclideans = squareform(pdist(cData, 'squaredeuclidean'));
s_sq_euclideans = squareform(pdist(sData, 'squaredeuclidean'));

% similarity/weighted adjacency matrices
c_sim = exp( c_sq_euclideans/(-2*(sigma^2)) );
s_sim = exp( s_sq_euclideans/(-2*(sigma^2)) );

% adjacency matrix is ones since fully connected
c_adj = ones(size(cData,1));
s_adj = ones(size(sData,1));


%% PART I - CALCULATE EIGENVALUES

[~,~,~,c_evals1] = sc1(c_sim, c_adj);
c_evals1 = diag(c_evals1);
figure;
scatter(1:size(c_sim,1), c_evals1, '.');
title('Eigenvalues of L for Circle data');

[~,~,~,c_evals2] = sc2(c_sim, c_adj);
c_evals2 = diag(c_evals2);
figure;
scatter(1:size(c_sim,1), c_evals2, '.');
title('Eigenvalues of Lrw for Circle data');

[~,~,~,c_evals3] = sc3(c_sim, c_adj);
c_evals3 = diag(c_evals3);
figure;
scatter(1:size(c_sim,1), c_evals3, '.');
title('Eigenvalues of Lsym for Circle datas');

[~,~,~,s_evals1] = sc1(s_sim, s_adj);
s_evals1 = diag(s_evals1);
figure;
scatter(1:size(s_sim,1), s_evals1, '.');
title('Eigenvalues of L for Spiral data');

[~,~,~,s_evals2] = sc2(s_sim, s_adj);
s_evals2 = diag(s_evals2); 
figure;
scatter(1:size(s_sim,1), s_evals2, '.');
title('Eigenvalues of Lrw for Spiral data');

[~,~,~,s_evals3] = sc3(s_sim, s_adj);
s_evals3 = diag(s_evals3);
figure;
scatter(1:size(s_sim,1), s_evals3, '.');
title('Eigenvalues of Lsym for Spiral data');


%% PART II - CLUSTER AND PLOT

%% CLUSTER ON CIRCLE DATA

% k = 2 clusters
[c_sc3_k2_assignments, c_sc3_k2_centroids, ~, ~] = sc3(c_sim, c_adj, 2);

% k = 3 clusters
[c_sc3_k3_assignments, c_sc3_k3_centroids, c_sc3_k3_evecs, ~] = sc3(c_sim, c_adj, 3);

% k = 4 clusters
[c_sc3_k4_assignments, c_sc3_k4_centroids, ~, ~] = sc3(c_sim, c_adj, 4);


%% GENERATE PLOTS FOR CIRCLE DATA

% plot k=2 assignments and centroids
figure;
scatter(cData(c_sc3_k2_assignments==1, 1), cData(c_sc3_k2_assignments==1, 2), 'filled', 'r');
hold on;
scatter(cData(c_sc3_k2_assignments==2, 1), cData(c_sc3_k2_assignments==2, 2), 'filled', 'b');
hold on;
scatter(c_sc3_k2_centroids(:,1), c_sc3_k2_centroids(:,2), 50, '*', 'k');

% set plot properties
legend('Cluster 1', 'Cluster 2', 'Cluster Centroids');
ax = gca;
ax.Title.String = 'Spectral Clustering (k=2) on Circle Data';
ax.XLabel.String = 'x1';
ax.YLabel.String = 'x2';

% plot k=3 assignments and centroids
figure;
scatter(cData(c_sc3_k3_assignments==1, 1), cData(c_sc3_k3_assignments==1, 2), 'filled', 'r');
hold on;
scatter(cData(c_sc3_k3_assignments==2, 1), cData(c_sc3_k3_assignments==2, 2), 'filled', 'b');
hold on;
scatter(cData(c_sc3_k3_assignments==3, 1), cData(c_sc3_k3_assignments==3, 2), 'filled', 'g');
hold on;
scatter(c_sc3_k3_centroids(:,1), c_sc3_k3_centroids(:,2), 50, '*', 'k');

% set plot properties
legend('Cluster 1', 'Cluster 2', 'Cluster 3', 'Cluster Centroids');
ax = gca;
ax.Title.String = 'Spectral Clustering (k=3) on Circle Data';
ax.XLabel.String = 'x1';
ax.YLabel.String = 'x2';

% plot k=4 assignments and centroids
figure;
scatter(cData(c_sc3_k4_assignments==1, 1), cData(c_sc3_k4_assignments==1, 2), 'filled', 'r');
hold on;
scatter(cData(c_sc3_k4_assignments==2, 1), cData(c_sc3_k4_assignments==2, 2), 'filled', 'b');
hold on;
scatter(cData(c_sc3_k4_assignments==3, 1), cData(c_sc3_k4_assignments==3, 2), 'filled', 'g');
hold on;
scatter(cData(c_sc3_k4_assignments==4, 1), cData(c_sc3_k4_assignments==4, 2), 'filled', 'k');
hold on;
scatter(c_sc3_k4_centroids(:,1), c_sc3_k4_centroids(:,2), 50, '*', 'k');

% set plot properties
legend('Cluster 1', 'Cluster 2', 'Cluster 3', 'Cluster 4', 'Cluster Centroids');
ax = gca;
ax.Title.String = 'Spectral Clustering (k=4) on Circle Data';
ax.XLabel.String = 'x1';
ax.YLabel.String = 'x2';


%% CLUSTER ON SPIRAL DATA

% k = 2 clusters
[s_sc3_k2_assignments, s_sc3_k2_centroids, ~, ~] = sc3(s_sim, s_adj, 2);

% k = 3 clusters
[s_sc3_k3_assignments, s_sc3_k3_centroids, s_sc3_k3_evecs, ~] = sc3(s_sim, s_adj, 3);

% k = 4 clusters
[s_sc3_k4_assignments, s_sc3_k4_centroids, ~, ~] = sc3(s_sim, s_adj, 4);


%% GENERATE PLOTS FOR SPIRAL DATA

% plot k=2 assignments and centroids
figure;
scatter(sData(s_sc3_k2_assignments==1, 1), sData(s_sc3_k2_assignments==1, 2), 'filled', 'r');
hold on;
scatter(sData(s_sc3_k2_assignments==2, 1), sData(s_sc3_k2_assignments==2, 2), 'filled', 'b');
hold on;
scatter(s_sc3_k2_centroids(:,1), s_sc3_k2_centroids(:,2), 50, '*', 'k');

% set plot properties
legend('Cluster 1', 'Cluster 2', 'Cluster Centroids');
ax = gca;
ax.Title.String = 'Spectral Clustering (k=2) on Spiral Data';
ax.XLabel.String = 'x1';
ax.YLabel.String = 'x2';

% plot k=3 assignments and centroids
figure;
scatter(sData(s_sc3_k3_assignments==1, 1), sData(s_sc3_k3_assignments==1, 2), 'filled', 'r');
hold on;
scatter(sData(s_sc3_k3_assignments==2, 1), sData(s_sc3_k3_assignments==2, 2), 'filled', 'b');
hold on;
scatter(sData(s_sc3_k3_assignments==3, 1), sData(s_sc3_k3_assignments==3, 2), 'filled', 'g');
hold on;
scatter(s_sc3_k3_centroids(:,1), s_sc3_k3_centroids(:,2), 50, '*', 'k');

% set plot properties
legend('Cluster 1', 'Cluster 2', 'Cluster 3', 'Cluster Centroids');
ax = gca;
ax.Title.String = 'Spectral Clustering (k=3) on Spiral Data';
ax.XLabel.String = 'x1';
ax.YLabel.String = 'x2';

% plot k=4 assignments and centroids
figure;
scatter(sData(s_sc3_k4_assignments==1, 1), sData(s_sc3_k4_assignments==1, 2), 'filled', 'r');
hold on;
scatter(sData(s_sc3_k4_assignments==2, 1), sData(s_sc3_k4_assignments==2, 2), 'filled', 'b');
hold on;
scatter(sData(s_sc3_k4_assignments==3, 1), sData(s_sc3_k4_assignments==3, 2), 'filled', 'g');
hold on;
scatter(sData(s_sc3_k4_assignments==4, 1), sData(s_sc3_k4_assignments==4, 2), 'filled', 'k');
hold on;
scatter(s_sc3_k4_centroids(:,1), s_sc3_k4_centroids(:,2), 50, '*', 'k');

% set plot properties
legend('Cluster 1', 'Cluster 2', 'Cluster 3', 'Cluster 4', 'Cluster Centroids');
ax = gca;
ax.Title.String = 'Spectral Clustering (k=4) on Spiral Data';
ax.XLabel.String = 'x1';
ax.YLabel.String = 'x2';


%% PART III - 3D PLOTS

% we already have evecs for SC3 run on circle, spiral. calculate the other evecs

% sc1 on circle data with k=3
[c_sc1_k3_assignments, ~, c_sc1_k3_evecs, ~] = sc1(c_sim, c_adj, 3);
figure;
scatter3(c_sc1_k3_evecs(c_sc1_k3_assignments==1,1), ...
        c_sc1_k3_evecs(c_sc1_k3_assignments==1,2), ...
        c_sc1_k3_evecs(c_sc1_k3_assignments==1,3), ...
        'filled', 'r');
hold on;
scatter3(c_sc1_k3_evecs(c_sc1_k3_assignments==2,1), ...
        c_sc1_k3_evecs(c_sc1_k3_assignments==2,2), ...
        c_sc1_k3_evecs(c_sc1_k3_assignments==2,3), ...
        'filled', 'b');
hold on;
scatter3(c_sc1_k3_evecs(c_sc1_k3_assignments==3,1), ...
        c_sc1_k3_evecs(c_sc1_k3_assignments==3,2), ...
        c_sc1_k3_evecs(c_sc1_k3_assignments==3,3), ...
        'filled', 'g');

% set plot properties
legend('Cluster 1', 'Cluster 2', 'Cluster 3');
ax = gca;
ax.Title.String = 'Rows of V matrix, SC-1 on Circle Data';
ax.XLabel.String = 'x1';
ax.YLabel.String = 'x2';
ax.ZLabel.String = 'x3';

% sc2 on circle data with k=3
[c_sc2_k3_assignments, ~, c_sc2_k3_evecs, ~] = sc2(c_sim, c_adj, 3);
figure;
scatter3(c_sc2_k3_evecs(c_sc2_k3_assignments==1,1), ...
        c_sc2_k3_evecs(c_sc2_k3_assignments==1,2), ...
        c_sc2_k3_evecs(c_sc2_k3_assignments==1,3), ...
        'filled', 'r');
hold on;
scatter3(c_sc2_k3_evecs(c_sc2_k3_assignments==2,1), ...
        c_sc2_k3_evecs(c_sc2_k3_assignments==2,2), ...
        c_sc2_k3_evecs(c_sc2_k3_assignments==2,3), ...
        'filled', 'b');
hold on;
scatter3(c_sc2_k3_evecs(c_sc2_k3_assignments==3,1), ...
        c_sc2_k3_evecs(c_sc2_k3_assignments==3,2), ...
        c_sc2_k3_evecs(c_sc2_k3_assignments==3,3), ...
        'filled', 'g');
    
% set plot properties
legend('Cluster 1', 'Cluster 2', 'Cluster 3');
ax = gca;
ax.Title.String = 'Rows of V matrix, SC-2 on Circle Data';
ax.XLabel.String = 'x1';
ax.YLabel.String = 'x2';
ax.ZLabel.String = 'x3';
    
% sc3 on circle data with k=3
figure;
scatter3(c_sc3_k3_evecs(c_sc3_k3_assignments==1,1), ...
        c_sc3_k3_evecs(c_sc3_k3_assignments==1,2), ...
        c_sc3_k3_evecs(c_sc3_k3_assignments==1,3), ...
        'filled', 'r');
hold on;
scatter3(c_sc3_k3_evecs(c_sc3_k3_assignments==2,1), ...
        c_sc3_k3_evecs(c_sc3_k3_assignments==2,2), ...
        c_sc3_k3_evecs(c_sc3_k3_assignments==2,3), ...
        'filled', 'b');
hold on;
scatter3(c_sc3_k3_evecs(c_sc3_k3_assignments==3,1), ...
        c_sc3_k3_evecs(c_sc3_k3_assignments==3,2), ...
        c_sc3_k3_evecs(c_sc3_k3_assignments==3,3), ...
        'filled', 'g');

% set plot properties
legend('Cluster 1', 'Cluster 2', 'Cluster 3');
ax = gca;
ax.Title.String = 'Rows of V matrix, SC-3 on Circle Data';
ax.XLabel.String = 'x1';
ax.YLabel.String = 'x2';
ax.ZLabel.String = 'x3';
    
% sc1 on spiral data with k=3
[s_sc1_k3_assignments, ~, s_sc1_k3_evecs, ~] = sc1(s_sim, s_adj, 3);
figure;
scatter3(s_sc1_k3_evecs(s_sc1_k3_assignments==1,1), ...
        s_sc1_k3_evecs(s_sc1_k3_assignments==1,2), ...
        s_sc1_k3_evecs(s_sc1_k3_assignments==1,3), ...
        'filled', 'r');
hold on;
scatter3(s_sc1_k3_evecs(s_sc1_k3_assignments==2,1), ...
        s_sc1_k3_evecs(s_sc1_k3_assignments==2,2), ...
        s_sc1_k3_evecs(s_sc1_k3_assignments==2,3), ...
        'filled', 'b');
hold on;
scatter3(s_sc1_k3_evecs(s_sc1_k3_assignments==3,1), ...
        s_sc1_k3_evecs(s_sc1_k3_assignments==3,2), ...
        s_sc1_k3_evecs(s_sc1_k3_assignments==3,3), ...
        'filled', 'g');

% set plot properties
legend('Cluster 1', 'Cluster 2', 'Cluster 3');
ax = gca;
ax.Title.String = 'Rows of V matrix, SC-1 on Spiral Data';
ax.XLabel.String = 'x1';
ax.YLabel.String = 'x2';
ax.ZLabel.String = 'x3';

% sc2 on spiral data with k=3
[s_sc2_k3_assignments, ~, s_sc2_k3_evecs, ~] = sc2(s_sim, s_adj, 3);
figure;
scatter3(s_sc2_k3_evecs(s_sc2_k3_assignments==1,1), ...
        s_sc2_k3_evecs(s_sc2_k3_assignments==1,2), ...
        s_sc2_k3_evecs(s_sc2_k3_assignments==1,3), ...
        'filled', 'r');
hold on;
scatter3(s_sc2_k3_evecs(s_sc2_k3_assignments==2,1), ...
        s_sc2_k3_evecs(s_sc2_k3_assignments==2,2), ...
        s_sc2_k3_evecs(s_sc2_k3_assignments==2,3), ...
        'filled', 'b');
hold on;
scatter3(s_sc2_k3_evecs(s_sc2_k3_assignments==3,1), ...
        s_sc2_k3_evecs(s_sc2_k3_assignments==3,2), ...
        s_sc2_k3_evecs(s_sc2_k3_assignments==3,3), ...
        'filled', 'g');

% set plot properties
legend('Cluster 1', 'Cluster 2', 'Cluster 3');
ax = gca;
ax.Title.String = 'Rows of V matrix, SC-2 on Spiral Data';
ax.XLabel.String = 'x1';
ax.YLabel.String = 'x2';
ax.ZLabel.String = 'x3';
    
% sc3 on spiral data with k=3
figure;
scatter3(s_sc3_k3_evecs(s_sc3_k3_assignments==1,1), ...
        s_sc3_k3_evecs(s_sc3_k3_assignments==1,2), ...
        s_sc3_k3_evecs(s_sc3_k3_assignments==1,3), ...
        'filled', 'r');
hold on;
scatter3(s_sc3_k3_evecs(s_sc3_k3_assignments==2,1), ...
        s_sc3_k3_evecs(s_sc3_k3_assignments==2,2), ...
        s_sc3_k3_evecs(s_sc3_k3_assignments==2,3), ...
        'filled', 'b');
hold on;
scatter3(s_sc3_k3_evecs(s_sc3_k3_assignments==3,1), ...
        s_sc3_k3_evecs(s_sc3_k3_assignments==3,2), ...
        s_sc3_k3_evecs(s_sc3_k3_assignments==3,3), ...
        'filled', 'g');

% set plot properties
legend('Cluster 1', 'Cluster 2', 'Cluster 3');
ax = gca;
ax.Title.String = 'Rows of V matrix, SC-3 on Spiral Data';
ax.XLabel.String = 'x1';
ax.YLabel.String = 'x2';
ax.ZLabel.String = 'x3';
