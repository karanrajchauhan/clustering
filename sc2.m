function [assignments, centroids, eigenvectors, eigenvalues] = sc2(sim_mat, adj_mat, k, seed)
    
    % if seed for rng is not specified, set it to 2
    if ~exist('seed','var')
        seed = 2;
    end

    % calculate weighted adjacency matrix
    w_adj = sim_mat .* adj_mat;
    
    % calculate degree matrix
    D = diag(sum(w_adj));
    
    % graph laplacian
    L = D - w_adj;
    
    % random walk normalized laplacian
    L_rw = D \ L;
    
    if ~exist('k','var')
        
        assignments = [];
        centroids = [];

        % return all the eigenvectors and eigenvalues
        [eigenvectors, eigenvalues] = eig(L_rw);
        
    else
        
        % compute the k smallest eigenvalues and corresponding eignvectors
        [eigenvectors, eigenvalues] = eigs(L_rw, k, 'SM');

        % for consistency
        rng(seed);

        % run kmeans on the rows of eigenvectors matrix
        [assignments, centroids] = kmeans(eigenvectors, k);
        
    end

end