%% LOAD CIRCLE DATA

% for consistency in results
rng(2);

% generate circle data with 3 clusters, 500 pts per cluster
[cData, cLabels] = sample_circle(3, [500 500 500]);


%% K-MEANS ON CIRCLE DATA

% k-means with 2 clusters
[ck2_assignments, ck2_centroids] = kmeans(cData, 2, 'Distance','sqeuclidean',...
    'Replicates',20);

% k-means with 3 clusters
[ck3_assignments, ck3_centroids] = kmeans(cData, 3, 'Distance','sqeuclidean',...
    'Replicates',20);

% k-means with 4 clusters
[ck4_assignments, ck4_centroids] = kmeans(cData, 4, 'Distance','sqeuclidean',...
    'Replicates',20);


%% GENERATE PLOTS FOR CIRCLE DATA

% plot k=2 assignments and centroids
figure;
scatter(cData(ck2_assignments==1, 1), cData(ck2_assignments==1, 2), 'filled', 'r');
hold on;
scatter(cData(ck2_assignments==2, 1), cData(ck2_assignments==2, 2), 'filled', 'b');
hold on;
scatter(ck2_centroids(:,1), ck2_centroids(:,2), 50, '*', 'k');

% set plot properties
legend('Cluster 1', 'Cluster 2', 'Cluster Centroids');
ax = gca;
ax.Title.String = 'k-means (k=2) on Circle Data';
ax.XLabel.String = 'x1';
ax.YLabel.String = 'x2';

% plot k=3 assignments and centroids
figure;
scatter(cData(ck3_assignments==1, 1), cData(ck3_assignments==1, 2), 'filled', 'r');
hold on;
scatter(cData(ck3_assignments==2, 1), cData(ck3_assignments==2, 2), 'filled', 'b');
hold on;
scatter(cData(ck3_assignments==3, 1), cData(ck3_assignments==3, 2), 'filled', 'g');
hold on;
scatter(ck3_centroids(:,1), ck3_centroids(:,2), 50, '*', 'k');

% set plot properties
legend('Cluster 1', 'Cluster 2', 'Cluster 3', 'Cluster Centroids');
ax = gca;
ax.Title.String = 'k-means (k=3) on Circle Data';
ax.XLabel.String = 'x1';
ax.YLabel.String = 'x2';

% plot k=4 assignments and centroids
figure;
scatter(cData(ck4_assignments==1, 1), cData(ck4_assignments==1, 2), 'filled', 'r');
hold on;
scatter(cData(ck4_assignments==2, 1), cData(ck4_assignments==2, 2), 'filled', 'b');
hold on;
scatter(cData(ck4_assignments==3, 1), cData(ck4_assignments==3, 2), 'filled', 'g');
hold on;
scatter(cData(ck4_assignments==4, 1), cData(ck4_assignments==4, 2), 'filled', 'k');
hold on;
scatter(ck4_centroids(:,1), ck4_centroids(:,2), 50, '*', 'k');

% set plot properties
legend('Cluster 1', 'Cluster 2', 'Cluster 3', 'Cluster 4', 'Cluster Centroids');
ax = gca;
ax.Title.String = 'k-means (k=4) on Circle Data';
ax.XLabel.String = 'x1';
ax.YLabel.String = 'x2';

%% CALCULATE WITHIN CLUSTER SUM OF SQUARES FOR CIRCLE DATA

c_wcss_k2 = zeros(2,1);
for c_num = 1:2
    c_wcss_k2(c_num, 1) = sum(sum((cData(ck2_assignments==c_num, :) - ck2_centroids(c_num, :)) .^2));
end

c_wcss_k3 = zeros(3,1);
for c_num = 1:3
    c_wcss_k3(c_num, 1) = sum(sum((cData(ck3_assignments==c_num, :) - ck3_centroids(c_num, :)) .^2));
end

c_wcss_k4 = zeros(4,1);
for c_num = 1:4
    c_wcss_k4(c_num, 1) = sum(sum((cData(ck4_assignments==c_num, :) - ck4_centroids(c_num, :)) .^2));
end


%% LOAD SPIRAL DATA

% generate spiral data with 3 clusters, 500 pts per cluster
[sData, sLabels] = sample_spiral(3, [500 500 500]);


%% K-MEANS ON SPIRAL DATA

% k-means with 2 clusters
[sk2_assignments, sk2_centroids] = kmeans(sData, 2, 'Distance','sqeuclidean',...
    'Replicates',20);

% k-means with 3 clusters
[sk3_assignments, sk3_centroids] = kmeans(sData, 3, 'Distance','sqeuclidean',...
    'Replicates',20);

% k-means with 4 clusters
[sk4_assignments, sk4_centroids] = kmeans(sData, 4, 'Distance','sqeuclidean',...
    'Replicates',20);


%% GENERATE PLOTS FOR SPIRAL DATA

% plot k=2 assignments and centroids
figure;
scatter(sData(sk2_assignments==1, 1), sData(sk2_assignments==1, 2), 'filled', 'r');
hold on;
scatter(sData(sk2_assignments==2, 1), sData(sk2_assignments==2, 2), 'filled', 'b');
hold on;
scatter(sk2_centroids(:,1), sk2_centroids(:,2), 50, '*', 'k');

% set plot properties
legend('Cluster 1', 'Cluster 2', 'Cluster Centroids');
ax = gca;
ax.Title.String = 'k-means (k=2) on Spiral Data';
ax.XLabel.String = 'x1';
ax.YLabel.String = 'x2';

% plot k=3 assignments and centroids
figure;
scatter(sData(sk3_assignments==1, 1), sData(sk3_assignments==1, 2), 'filled', 'r');
hold on;
scatter(sData(sk3_assignments==2, 1), sData(sk3_assignments==2, 2), 'filled', 'b');
hold on;
scatter(sData(sk3_assignments==3, 1), sData(sk3_assignments==3, 2), 'filled', 'g');
hold on;
scatter(sk3_centroids(:,1), sk3_centroids(:,2), 50, '*', 'k');

% set plot properties
legend('Cluster 1', 'Cluster 2', 'Cluster 3', 'Cluster Centroids');
ax = gca;
ax.Title.String = 'k-means (k=3) on Spiral Data';
ax.XLabel.String = 'x1';
ax.YLabel.String = 'x2';

% plot k=4 assignments and centroids
figure;
scatter(sData(sk4_assignments==1, 1), sData(sk4_assignments==1, 2), 'filled', 'r');
hold on;
scatter(sData(sk4_assignments==2, 1), sData(sk4_assignments==2, 2), 'filled', 'b');
hold on;
scatter(sData(sk4_assignments==3, 1), sData(sk4_assignments==3, 2), 'filled', 'g');
hold on;
scatter(sData(sk4_assignments==4, 1), sData(sk4_assignments==4, 2), 'filled', 'k');
hold on;
scatter(sk4_centroids(:,1), sk4_centroids(:,2), 50, '*', 'k');

% set plot properties
legend('Cluster 1', 'Cluster 2', 'Cluster 3', 'Cluster 4', 'Cluster Centroids');
ax = gca;
ax.Title.String = 'k-means (k=4) on Spiral Data';
ax.XLabel.String = 'x1';
ax.YLabel.String = 'x2';


%% CALCULATE WITHIN CLUSTER SUM OF SQUARES FOR SPIRAL DATA

s_wcss_k2 = zeros(2,1);
for c_num = 1:2
    s_wcss_k2(c_num, 1) = sum(sum((sData(sk2_assignments==c_num, :) - sk2_centroids(c_num, :)) .^2));
end

s_wcss_k3 = zeros(3,1);
for c_num = 1:3
    s_wcss_k3(c_num, 1) = sum(sum((sData(sk3_assignments==c_num, :) - sk3_centroids(c_num, :)) .^2));
end

s_wcss_k4 = zeros(4,1);
for c_num = 1:4
    s_wcss_k4(c_num, 1) = sum(sum((sData(sk4_assignments==c_num, :) - sk4_centroids(c_num, :)) .^2));
end
