function [assignments, centroids, eigenvectors, eigenvalues] = sc1(sim_mat, adj_mat, k, seed)
    
    % if seed for rng is not specified, set it to 2
    if ~exist('seed','var')
        seed = 2;
    end

    % calculate weighted adjacency matrix
    w_adj = sim_mat .* adj_mat;
    
    % calculate degree matrix
    D = diag(sum(w_adj));
    
    % graph laplacian
    L = D - w_adj;
    
    % todo: is 'sm' suppported in old and new versions?

    
    if ~exist('k','var')
        
        assignments = [];
        centroids = [];
        
        % return all the eigenvectors and eigenvalues
        [eigenvectors, eigenvalues] = eig(L);
        
    else
        
        % compute the k smallest eigenvalues and corresponding eignvectors
        [eigenvectors, eigenvalues] = eigs(L, k, 'SM');

        % for consistency
        rng(seed);

        % run kmeans on the rows of eigenvectors matrix
        [assignments, centroids] = kmeans(eigenvectors, k);
        
    end

end